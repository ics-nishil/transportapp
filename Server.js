const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const routes = require('./routes/TransportAppRoutes');
const session = require('express-session');
require("dotenv").config();


const app = express();
const PORT = process.env.PORT || 8080;

//Middleware
app.use(express.json())
app.use(bodyParser.json());
app.use(session({
    secret: process.env.SESSION_SECRET, 
    resave: false,
    saveUninitialized: false,
    cookie: {
        maxAge: 1000 * 60 * 5
    }
}));


mongoose.connect(process.env.MONGO_URI)
.then(() => console.log("MongoDB connected..."))
.catch(err => console.log(err));

app.use("/api", routes);

app.listen(PORT, ()=>{
    console.log(`Listening at ${PORT}...`)
})