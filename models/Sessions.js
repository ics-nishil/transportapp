const mongoose = require("mongoose");

const sessionSchema = new mongoose.Schema({
   
    user_id:{
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'Owner'
    },
    session_id:{
        type: Object,
        required: true
    },
    jwt:{
        type: String,
        required: true
    }
   
});

module.exports = mongoose.model('session', sessionSchema);