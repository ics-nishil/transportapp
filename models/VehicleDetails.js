const mongoose = require("mongoose");

const vehicleDetailsSchema = new mongoose.Schema({
    vehicleNumber:{
        type: String, 
        uppercase: true,
        required: true,
    },
    vehicleType:{
        type: String,
        required: true,
    },
    vehicleCategory:{
        type: String,
        required: true,
    },
    vehicleCompany:{
        type: String,
        required: true,
    },
    vehicles:{
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'Owner' 
    }
});

module.exports = mongoose.model('Vehicle', vehicleDetailsSchema);