const mongoose = require("mongoose");

const transportOwnerSchema = new mongoose.Schema({
    name:{
        type: String,
        required: true,
    },
    gender:{
        type: String, 
        required: true,
    },
    age:{
        type: Number,
        required: true,
    },
    email:{
        type: String,
        unique: true,
        required: true,
    },
    password:{
        type: String,
        required: true,
    }
});

module.exports = mongoose.model('Owner', transportOwnerSchema);