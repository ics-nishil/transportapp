const yup = require("yup");

module.exports.addVehicle = yup.object({
  vehicleNumber: yup.string().max(10).uppercase().required(),
  vehicleType: yup.string().required(),
  vehicleCategory: yup.string().required(),
  vehicleCompany: yup.string().required()
});

module.exports.updateVehicle = yup.object({
  vehicleNumber: yup.string().max(10),
  vehicleType: yup.string(),
  vehicleCategory: yup.string(),
  vehicleCompany: yup.string()
});
