const yup = require("yup");

module.exports.userSchema = yup.object({
  name: yup.string().required(),
  gender: yup.string().required(),
  age: yup.number().min(12).required(),
  email: yup.string().email().required(),
  password: yup.string().min(3).max(10).required()
});


module.exports.userLogin = yup.object({
  email: yup.string().email().required(),
  password: yup.string().min(3).max(10).required()
});


module.exports.updateOwner = yup.object({
  name: yup.string(),
  gender: yup.string(),
  age: yup.number().min(12),
  password: yup.string().min(3).max(10)
});