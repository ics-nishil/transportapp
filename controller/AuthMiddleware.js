const jwt = require('jsonwebtoken');
const SessionSchema = require("../models/Sessions");

module.exports.auth =  async(req, res, next) => {

try{
 
    let token = req.headers['authorization'];
    
    if(token){
        token = token.split(' ')[1];
        
        const decode = jwt.verify(token, process.env.JWT_SECRET_KEY);

        // Check if session is active
        const activeSession = await SessionSchema.find({ user_id: decode.id, jwt: token });

        if(!activeSession[0].session_id.is_login){
            throw new Error("Unauthorized User !!!");
        } 


        req.data={
            login: true,
            data: decode
        }
        next();
    }else{
        res.json({
            login: false,
            data: {msg: 'You Are Not Authorized To This Page'}
        });
    }
}catch (error) {
    res.status(401).json({
        message: error.message
    });
}

};