const VehicleDetails = require("../models/VehicleDetails");
const mongoose = require("mongoose");

module.exports.getVehicleDetails = async(req,res) => {    
    const {id} = req.params;
    const data = new mongoose.Types.ObjectId(id);
    
    const vehiclesDetails = await VehicleDetails.find({vehicles: data});
    res.send(vehiclesDetails);
};



module.exports.addVehicleDetails = async(req,res) => {
    const {id} = req.params;

    const vehicle = {
        vehicleNumber: req.body.vehicleNumber,
        vehicleType: req.body.vehicleType,
        vehicleCategory: req.body.vehicleCategory,
        vehicleCompany: req.body.vehicleCompany,
        vehicles: new mongoose.Types.ObjectId(id)
    };

    VehicleDetails.create(vehicle).then((data)=>{
        console.log("Vehicle Added Successfully....");
        res.status(201).send(data);
    }).catch((err) => {
        console.log(err);
        res.send({error: err, msg: "Something went wrong"}); 
    });
};



module.exports.updateVehicleDetails = (req,res) => {
    const vehicle = req.body;
    const {id} = req.params;
    
    VehicleDetails.findByIdAndUpdate(id, vehicle).then(() => {
        res.send("Vehicle Details Updated Successfully...");
    }).catch((err) => {
        console.log(err);
        res.send({error: err, msg: "Something went wrong"}); 
    });
};



module.exports.deleteVehicleDetails = (req,res) => {
    const {id} = req.params;

    VehicleDetails.findByIdAndDelete(id).then(() => {
        res.send("Vehicle Details Deleted Successfully...");
    }).catch((err) => {
        console.log(err);
        res.send({error: err, msg: "Something went wrong"}); 
    });
};