const TransportOwner = require("../models/TransportOwner");
const VehicleDetails = require("../models/VehicleDetails");
const mongoose = require("mongoose");
const bcrypt = require('bcrypt');

module.exports.getOwnerDetails = async(req,res) => {
    const credentials = req.data;
    const ownerDetails = await TransportOwner.find();
    res.send({credentials, ownerDetails});
};


module.exports.updateOwnerDetails = async(req,res) => {
    const hashPassword = await bcrypt.hash(req.body.password,12);
    const {id} = req.params;
    const owner = {
        name: req.body.name,
        gender: req.body.gender,
        age: req.body.age,
        password: hashPassword
    };
    TransportOwner.findByIdAndUpdate(id, owner).then(() => {
        res.send("Owner Details Updated Successfully...");
    }).catch((err) => {
        console.log(err);
        res.send({error: err, msg: "Something went wrong"}); 
    });
};


module.exports.deleteOwnerDetails = async(req,res) => {
    try {
        const {id} = req.params;
        const data = new mongoose.Types.ObjectId(id);
    
        const isDelete = await TransportOwner.findByIdAndDelete(id);
        
        if (isDelete) {
          await VehicleDetails.deleteMany({ vehicles: data }); 
        }
        
        res.status(200).send("Owner Data Deleted Successfully !!!!");
      } catch(err) {
        console.error(err.message);
        res.status(400).send("Server Error");
      }
    
};






// module.exports.createOwnerDetails = async(req,res) => {
//     const {name, phoneNumber, age} = req.body;
   
//     const vehicle = await VehicleDetails.create({
//         vehicleNumber: req.body.vehicleNumber,
//         vehicleType: req.body.vehicleType,
//         vehicleCategory: req.body.vehicleCategory,
//         vehicleCompany: req.body.vehicleCompany,
//     });

//     const objID = new mongoose.Types.ObjectId(vehicle.id);

//     const owner = {
//         name: name,
//         phoneNumber: phoneNumber,
//         age: age,
//         vehicles: objID
//     };

//     TransportOwner.create(owner).then((data)=>{
//         console.log("Owner Created Successfully....");
//         res.status(201).send(data);
//     }).catch((err) => {
//         console.log("Error: " + err);
//         res.send({error: err, msg: "Something went wrong"});
//     });

// };








// module.exports.deleteVehicleDetails = (req,res) => {
//     const {id} = req.params;

//     VehicleDetails.findByIdAndDelete(id).then(() => {
//         res.send("Vehicle Details Deleted Successfully...");
//     }).catch((err) => {
//         console.log(err);
//         res.send({error: err, msg: "Something went wrong"}); 
//     });
// };


// module.exports.addVehicleDetails = async(req,res) => {
//     const {id} = req.params;
//     const vehicle = await VehicleDetails.create(req.body);    
    
//     const objID = new mongoose.Types.ObjectId(vehicle.id);

//     TransportOwner.updateOne(
//         {
//             _id: id
//         }, 
//         {
//             $push:{ vehicles: objID }
//         }
//     ).then(() => {
//         res.send("Vehicle Added Successfully...")
//     }).catch((err) => {
//         console.log(err);
//         res.send({error: err, msg: "Something went wrong"}); 
//     });
// };

// module.exports.updateVehicleDetails = (req,res) => {
//     const vehicle = req.body;
//     const {id} = req.params;
    
//     VehicleDetails.findByIdAndUpdate(id, vehicle).then(() => {
//         res.send("Vehicle Details Updated Successfully...");
//     }).catch((err) => {
//         console.log(err);
//         res.send({error: err, msg: "Something went wrong"}); 
//     });
// };

