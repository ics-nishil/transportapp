const Users = require("../models/TransportOwner");
const SessionSchema = require("../models/Sessions");
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const os = require('os');
const mongoose = require('mongoose');

module.exports.register = async(req,res) => {
    const { name, age, gender, email, password } = req.body;
    
    const securedPassword = await bcrypt.hash(password, 12);
    
    const user = {
        name: name,
        age: age,
        gender: gender,
        email: email,
        password: securedPassword
    }

    // const devicePlatform = os.platform();

    try {
            const existingUser = await Users.findOne({ email });
            
            if (existingUser) {
                return res.status(400).json({ message: 'Email already exists' });
            }

            const newUser = new Users(user);
            await newUser.save();
        
            //req.session.username = newUser.name;
            
        
            const token = jwt.sign({ email, id: user._id },  process.env.JWT_SECRET_KEY, {expiresIn: process.env.JWT_EXPIRE_IN});
        
            // const session = new SessionSchema({
            //     user_id: new mongoose.Types.ObjectId(newUser._id),
            //     is_login: true,
            //     platform: devicePlatform,
            //     session_id: req.sessionID,
            //     jwt: token
            // });
            // await session.save();
        
            res.status(201).json({ user: newUser, token });
  
        } 
    catch (error) {
        console.error(error);
        res.status(500).json({ message: 'Internal Server Error' });
    }

}



module.exports.login = async(req,res) => {
    const { email, password } = req.body;
    const devicePlatform = os.platform();

    try {
        const user = await Users.findOne({ email });
        
        if (!user) {
            return res.status(400).json({ message: 'User not found' });
        }
        
        const hashPassword = await bcrypt.compare(password, user?.password);        
        
        if (!hashPassword) {
            return res.status(401).json({ message: 'Invalid email or password' });
        }
        
        // Check for active session
        
        const activeSession = await SessionSchema.findOne({ 
            user_id : user._id, 
            'session_id.is_login' : true
        });
        
        
        if (activeSession) {
            await SessionSchema.findByIdAndUpdate(activeSession._id, { $set: {'session_id.is_login' : false}});
        }

        req.session.username = user.name;
        req.session.is_login = true;
        req.session.platform = devicePlatform;
        
        const token = jwt.sign({ email, id: user._id }, process.env.JWT_SECRET_KEY, {expiresIn: process.env.JWT_EXPIRE_IN});
       
        // Store session in the database
        await SessionSchema.create({ 
            user_id: user._id,
            session_id: req.session,
            jwt: token 
        });
        // console.log(req.session);
        
        res.status(200).json({ 
            user, 
            token 
        });

        
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: 'Internal Server Error' });
    }
}


// const token = jwt.sign({ email }, process.env.JWT_SECRET_KEY);
// console.log( req.cookies.uid );

// res.cookie("uid", token);
// res.send("hi");