const {Router} = require("express");
const {getOwnerDetails, deleteOwnerDetails, updateOwnerDetails} = require("../controller/OwnerController");
const {getVehicleDetails, addVehicleDetails, updateVehicleDetails, deleteVehicleDetails} = require("../controller/VehicleController");
const {login, register} = require("../controller/AuthController");
const {auth} = require("../controller/AuthMiddleware");
const validation = require("../Middlewares/validationMiddleware");
const {userSchema, userLogin, updateOwner} = require("../Validations/userValidation");
const {addVehicle, updateVehicle} = require("../Validations/vehicleValidation");

const router = Router();

router.post('/login', validation(userLogin),login);
router.post('/register', validation(userSchema), register);
router.post('/add/:id', validation(addVehicle), addVehicleDetails);


router.get('/vehicles/:id', auth, getVehicleDetails);
router.get('/get', auth, getOwnerDetails);


router.put('/update-vehicle/:id', validation(updateVehicle), updateVehicleDetails);
router.put('/update-owner/:id', validation(updateOwner), updateOwnerDetails);


router.delete('/delete-vehicle/:id', deleteVehicleDetails);
router.delete('/delete-owner/:id', deleteOwnerDetails)

module.exports = router;